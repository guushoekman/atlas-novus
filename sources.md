---
layout: base
title: Bronnen
subtitle: Databronnen
---
<section class="section">
	<div class="container content">
		<h2>Kaarten van heel Nederland</h2>
		<p>
			De data voor deze kaarten komt allemaal van <a href="https://www.pdok.nl">PDOK</a>, specifiek van hun <a href="https://www.pdok.nl/introductie/-/article/basisregistratie-topografie-brt-topnl">Basisregistratie Topografie TOPNL dataset</a>. Afhankelijk van de grootte van de kaart heb ik TOP10NL, TOP50NL of TOP100NL gebruikt.
		</p>
		<p>
			Deze datasets zijn hebben een <a href="https://creativecommons.org/licenses/by/4.0/deed.nl">CC-BY-4.0 licentie</a> en worden met enige regelmaat geüpdate. Ik heb deze kaarten gebruikt met data van juni 2018.
		</p>
		<h2>Bomen in steden</h2>
		<p>Voor deze kaarten is er per stad een andere dataset gebruikt. Zie de tabel hieronder.</p>
		<p>De tegels van de kaart zijn gemaakt door <a href="https://carto.com/location-data-services/basemaps/">Carto</a> en heeft een <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a> licentie. De kaartdata is van <a href="https://www.openstreetmap.org/">OpenStreetMap</a> en heeft een <a href="https://opendatacommons.org/licenses/odbl/summary/index.html">ODbL</a> licentie.</p>
		<table class="table is-fullwidth">
			<thead>
				<tr>
					<th>Stad</th>
					<th>Licentie</th>
				</tr>
			</thead>
			<tbody>
				{% for map in site.trees %}
				<tr>
					<td><a href="{{ map.source }}">{{ map.title }}</a></td>
					<td><a href="{{ map.license_source }}">{{ map.license }}</a></td>
				</tr>
				{% endfor %}
			</tbody>
		</table>
	</div>
</section>