// show download options
$(".card-footer.sizes .card-footer-item").click(function(e) {
  e.preventDefault();
  $(this).parent().prev(".card-footer.download").addClass("visible");
  $(this).siblings().removeClass("active");
  $(this).addClass("active");
  $(this).parent().prev().children().children(".size").text($(this).attr("size"));
  $(this).parent().prev().children().children(".width").text($(this).attr("width"));
  $(this).parent().prev().children().children(".height").text($(this).attr("height"));
  $(this).parent().prev().children().children("a").attr("size", ($(this).text()));
  $(this).parent().prev().children().children("a").attr("href", ($(this).attr("url")));
})

// google analytics on downloading map
$(".download.button").click(function() {
  var map = $(this).attr("map");
  var size = $(this).attr("size");
  gtag('event', 'download', {
    'event_category': map,
    'event_label': size,
  });
})

// show alternate image when clicking on map image
$(".maps .card.map .card-content img").click(function() {
  var original =  $(this).attr("original");
  var alternative =  $(this).attr("alternative");
  var title = $(this).attr("alt");

  if ($(this).hasClass("alternative")) {
    $(this).attr("src", original)
  } else {
    gtag('event', 'zoom in', {
      'event_category': title,
    });
    $(this).attr("src", alternative)
  };

  $(this).toggleClass("alternative");
});

// tabs on homepage
$(".tabs li[data-tab]").click(function() {
  var tab = $(this).data("tab");

  $(".tabs li[data-tab]").removeClass("is-active");
  $(this).addClass("is-active");

  $("section[data-tab]").removeClass("is-active");
  $("section[data-tab='" + tab + "']").addClass("is-active");
})
