---
layout: base
subtitle: "Kaarten van Nederland, volledig vrij te gebruiken"
---
<div class="container">
  <div class="tabs">
    <ul>
      <li class="is-active" data-tab="country"><a>Heel Nederland</a></li>
      <li data-tab="trees"><a>Bomen in steden</a></li>
    </ul>
  </div>
</div>

<section class="section maps is-active" data-tab="country">
  <div class="container">
    <div class="columns is-multiline">
      {% for map in site.country %}
      <div class="column is-half-tablet is-one-third-desktop">
        <div class="card map">
          <header class="card-header">
            <p class="card-header-title">
              {{ map.title }}
            </p>
          </header>
          <div class="card-content">
            <div class="content">
              <img src="img/country/{{ map.name }}-thumb.png" original="img/country/{{ map.name }}-thumb.png" alternative="img/country/{{ map.name }}-alt.png" alt="{{ map.title }}">
            </div>
          </div>
          <footer class="card-footer warning">
            <div class="card-footer-item">
              <span>Waarschuwing: grote afbeeldingen kunnen je computer laten vastlopen</span>
            </div>
          </footer>
          <footer class="card-footer download">
            <div href="#" class="card-footer-item">
              <span class="size"></span>
              |
              <span class="width"></span>
              ×
              <span class="height"></span>
              <a map="{{ map.title }}" size="" href="" target="_blank" class="download button is-small is-link">Download</a>
            </div>
          </footer>
          <footer class="card-footer sizes">
            {% for size in map.sizes %}
            <a href="#" size="{{ size.size }} MB" width="{{ size.width }}" height="{{ size.height }}" url="{{ size.url }}" class="card-footer-item {% if size.size > 40 %}warning{% endif %}">{{ size.title }}</a>
            {% endfor %}
          </footer>
        </div>
      </div>
      {% endfor %}
    </div>
  </div>
</section>

<section class="section maps" data-tab="trees">
  <div class="container">
    <div class="columns is-multiline">
      {% for map in site.trees %}
      <div class="column is-half-tablet is-one-third-desktop">
        <div class="card map">
          <header class="card-header">
            <p class="card-header-title">
              {{ map.title }}
            </p>
          </header>
          <div class="card-content">
            <div class="content">
              <img src="img/trees/{{ map.name }}-thumb.png" original="img/trees/{{ map.name }}-thumb.png" alternative="img/trees/{{ map.name }}-alt.png" alt="{{ map.title }}">
            </div>
          </div>
          <footer class="card-footer download">
            <div href="#" class="card-footer-item">
              <span class="size"></span>
              |
              <span class="width"></span>
              ×
              <span class="height"></span>
              <a map="{{ map.title }}" size="" href="" target="_blank" class="download button is-small is-link">Download</a>
            </div>
          </footer>
          <footer class="card-footer sizes">
            {% for size in map.sizes %}
            <a href="#" size="{{ size.size }} MB" width="{{ size.width }}" height="{{ size.height }}" url="{{ size.url }}" class="card-footer-item {% if size.size > 40 %}warning{% endif %}">{{ size.title }}</a>
            {% endfor %}
          </footer>
        </div>
      </div>
      {% endfor %}
    </div>
  </div>
</section>