---
title: Delft
name: delft
source: https://data.overheid.nl/dataset/bomen-in-beheer-door-gemeente-delft
license: CC-0
license_source: https://creativecommons.org/publicdomain/zero/1.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1RHDEafHYiVxon7YnOoTzNcbMiBK2_f5E
    size: 1.3
    width: 825
    height: 1100
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=1kWk-TznKlW_WhGwklIXvwW7kJM_2Vwww
    size: 4.3
    width: 1650
    height: 2200
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=17_1mPJ5CAdS8xQtBIjuaRFXnpQvspt0J
    size: 12.5
    width: 3300
    height: 4400
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=1ScNU859mzY7qprrxLJz9poRbr5yQ0hef
    size: 32.2
    width: 6600
    height: 8800
---