---
title: Arnhem
name: arnhem
source: https://data.overheid.nl/dataset/bomenkaart-arnhem
license: CC-BY
license_source: https://creativecommons.org/licenses/by/4.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1ejTir5Ewaqj_l1OSaQ9gOhpTgpkMhf1N
    size: 0.8
    width: 875
    height: 750
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=160Q1CgIJPCwUo4Pruv4L3PjErKwAffom
    size: 2.7
    width: 1750
    height: 1500
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1OCCdhchW67Jaqc-q36j28FOgRnQchqU1
    size: 9.0
    width: 3500
    height: 3000
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=1wX-xS2svQJISrYpJvXZfxx10jdESo_hr
    size: 25.5
    width: 7000
    height: 6000
---