---
title: Amersfoort
name: amersfoort
source: https://data.overheid.nl/dataset/amersfoort-gemeentelijke_bomen
license: CC-0
license_source: https://creativecommons.org/publicdomain/zero/1.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=12YWeLsXC1RDUKGg_qsauDSYeahoPRvMB
    size: 1.5
    width: 1000
    height: 1200
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=1IOZ_161_kp8mwWKvbjxNqZCwEwLhlE0E
    size: 5.0
    width: 2000
    height: 2400
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1Y5V4S5rFy_X63XtsRiGvn2mD70Uyfb4d
    size: 14.7
    width: 4000
    height: 4800
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=1VFIOkZRSpr2mBzqXmlQl8yBSY3QlJzvm
    size: 37.6
    width: 8000
    height: 9600
---