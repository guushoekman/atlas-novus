---
title: Lelystad
name: lelystad
source: https://data.overheid.nl/dataset/bomen-lelystad
license: CC-BY
license_source: https://creativecommons.org/licenses/by/4.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1JGjHkVMv3rAJlaybmLAQoGrh1ogHzRCp
    size: 0.8
    width: 1000
    height: 1000
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=1qftmpCmgUQRdX_AQoR8UV1FrqeXCxXym
    size: 2.7
    width: 2000
    height: 2000
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1c7Nt70P843CFGt0bDdihsTaMDKeWDdQO
    size: 7.3
    width: 4000
    height: 4000
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=1JOZI_h5YBghebQAzFaGqoFx29nsUEFa7
    size: 17.3
    width: 8000
    height: 8000
---