---
title: Zaanstad
name: zaanstad
source: https://data.overheid.nl/dataset/znstdor15o
license: CC-0
license_source: https://creativecommons.org/publicdomain/zero/1.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1doSKHXCGw2dgjaItWWv10Xzq7lXwjvGY
    size: 1.3
    width: 1050
    height: 1000
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=1am2JoUn-cS30Fc27i8oZUbv_g3IHoTLt
    size: 4.1
    width: 2100
    height: 2000
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1s73OCQe-GDLY59E0TTKIq9yg9WGYQLRx
    size: 12.3
    width: 4200
    height: 4000
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=1XD4eG4xNC1vtSAjI4VTub6fGAujyJmd3
    size: 33.9
    width: 8400
    height: 8000
---