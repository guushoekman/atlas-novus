---
title: Den Haag
name: denhaag
source: https://data.overheid.nl/dataset/bomen-csv
license: CC-BY
license_source: https://creativecommons.org/licenses/by/4.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1zpLw9jOiyOsMNBUwOeattDK98wPaCPIQ
    size: 1.7
    width: 1300
    height: 1000
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=1P5Q6iG7BMu4GtYUtBAmMcCdAsVR8FD0I
    size: 5.9
    width: 2600
    height: 2000
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1dpip5nuXpF8hTEUSOzFB6GwB9a_PXIQB
    size: 20.2
    width: 5200
    height: 4000
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=1SVdKcEdT8pJQ6cMDhm8w12rV6H7EpA-X
    size: 59.5
    width: 10400
    height: 8000
---