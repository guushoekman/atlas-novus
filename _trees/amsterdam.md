---
title: Amsterdam
name: amsterdam
source: https://maps.amsterdam.nl/open_geodata/?k=254
license: "'een licentie voor het gebruiken en hergebruiken van de gedownloade dataset ... zowel voor niet-commerciële als commerciële doeleinden'"
license_source: https://maps.amsterdam.nl/open_geodata/terms.php?LANG=nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1_M0HDoELtQDvtHUaUuglShagUWVAD3Z_
    size: 1.9
    width: 1200
    height: 1080
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=11jB3cpxH43Fkxb7HhKtR0H009HJ2YOKb
    size: 6.6
    width: 2400
    height: 2160
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1DH2lo8pQLGE5XV-KMAn0T_OE6w4k7kTX
    size: 21.6
    width: 4800
    height: 4320
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=10Zh5oAM5IjbC4UlF3AflkCfdX_OIGUIS
    size: 61.8
    width: 9600
    height: 8640
---