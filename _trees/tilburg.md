---
title: Tilburg
name: tilburg
source: https://data.overheid.nl/dataset/bomen-tilburg
license: CC-0
license_source: https://creativecommons.org/publicdomain/zero/1.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1qAQ5iImQGlLMQZGvMpEEANq4D7_ecXdw
    size: 0.9
    width: 1200
    height: 1080
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=1AzG6xE_VcR9j3ExWCV2rwkIMkkEe6WS-
    size: 3.0
    width: 2000
    height: 1800
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1Bvgj2p5ouuje14s_-KP-KlfJCTTtHbpA
    size: 9.3
    width: 6000
    height: 4000
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=19x3z_41UI8c5XGbG3UONaOWJqQ2pRJcT
    size: 26.5
    width: 12000
    height: 8000
---