---
title: Eindhoven
name: eindhoven
source: https://data.overheid.nl/dataset/bomen-eindhoven
license: CC-BY
license_source: https://creativecommons.org/licenses/by/4.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1qn2y4mSV82Avt0k53Qp_OGUcnvGVTbly
    size: 1.4
    width: 1000
    height: 1000
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=1NSM42bhhb16ZXvht8Geua6MlHIJrsaGx
    size: 4.8
    width: 2000
    height: 2000
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1Q0EirFx5y_Vgzer4V07l0b77_ppnFryW
    size: 15.6
    width: 4000
    height: 4000
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=1BYQ_R-giPmy9Dp8rubopnGhW4COszCnL
    size: 43.5
    width: 8000
    height: 8000
---