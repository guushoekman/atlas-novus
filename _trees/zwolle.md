---
title: Zwolle
name: zwolle
source: http://opendata-zwolle.opendata.arcgis.com/datasets/2a37e79730854337931963cddb75bc81_2
license: CC-0
license_source: https://creativecommons.org/publicdomain/zero/1.0/deed.nl
sizes:
  - title: Klein
    url: https://drive.google.com/uc?export=download&id=1-iji-rcnflWC-7quKzRG4c1PJLJm1OKJ
    size: 1.4
    width: 1000
    height: 1000
  - title: Middel
    url: https://drive.google.com/uc?export=download&id=1LzWvvo_mIymVwhjwC4wwEI63OeCiZI5v
    size: 4.8
    width: 2000
    height: 2000
  - title: Groot
    url: https://drive.google.com/uc?export=download&id=1DbjlvY2zztMqgwyMmZEFvt-9h1JodQy0
    size: 15.6
    width: 4000
    height: 4000
  - title: Poster
    url: https://drive.google.com/uc?export=download&id=1Bhd1iXY0nq1nZFfq62VTaz-CkJqzfQ66
    size: 43.5
    width: 8000
    height: 8000
---